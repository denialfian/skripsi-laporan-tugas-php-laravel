<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/chart', function (Request $request) {
    $dates = explode('-', $request->dates);
    $date_from = date('Y-m-d', strtotime(trim($dates[0])));
    $date_to = date('Y-m-d', strtotime(trim($dates[1])));
    $query = "
    SELECT
        trx.tanggal,
        trx.total_trx,
        kategoris.nama_kategori
    FROM kategoris
    left join (
        SELECT
            date_range.tanggal,
            count(transaksis.id) as total_trx,
            transaksis.kategori_id
        FROM (
            select * from 
            (select adddate('" . $date_from . "',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) tanggal from
            (select 0 t0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
            (select 0 t1 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
            (select 0 t2 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
            (select 0 t3 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
            (select 0 t4 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
            where tanggal between '" . $date_from . "' and '" . $date_to . "'
        ) AS date_range
        left join transaksis on transaksis.tgl_transaksi = date_range.tanggal
        left join products on products.id = transaksis.kd_barang
        left join kategoris on kategoris.id = transaksis.kategori_id
        group by date_range.tanggal, transaksis.kategori_id
    ) as trx on trx.kategori_id = kategoris.id
    ";

    $res = DB::select($query);
    return $res;
});
